import {AngularFirestore} from 'angularfire2/firestore';
import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  array: Lenguaje[]=[];
  name:string;
  lenguaje:string="";

  public inputVal:string;
  constructor(private fire: AngularFirestore, public http: HttpClient, public modalController: ModalController) {

  }

  consulta(){
    // alert(this.inputVal);
    this.array=[];
      this.http.get("https://api.github.com/search/repositories?q="+this.inputVal).subscribe((data)=>{
        this.lenguaje = JSON.stringify(data);
      var objLen = JSON.parse(this.lenguaje);

      for (let i = 0; i < objLen.items.length; i++) {
        if (i <= 14) {
          this.array.push({
            avatar_url: objLen.items[i].owner.avatar_url,
            name: objLen.items[i].name,
            language: objLen.items[i].language,
            url: objLen.items[i].url,
            created_at: objLen.items[i].created_at,
            login: objLen.items[i].owner.login,
            contributors_url: objLen.items[i].contributors_url
          });
        }
      }
      });
    }
    async abrir_modal(index: number) {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {
        strJson: this.lenguaje,
        index: index
      }
    });
    await modal.present();

}
}
interface Lenguaje {
  avatar_url?: string;
  name?: string;
  language?: string;
  description?: string;
  url?: string;
  created_at?: string;
  login?: string;
  contributors_url?: string;
}
